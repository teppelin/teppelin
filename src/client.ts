import { GraphQLClient } from "graphql-request-native";
import { useEffect, useState } from "preact/hooks";
import { Variables } from "graphql-request-native/dist/esm/types";
const client = new GraphQLClient("https://teppel.in/graphql");

function useGraphql(
  query: string,
  variables?: Variables,
  // eslint-disable-next-line
): [Record<string, any>, boolean, boolean] {
  const [response, setResponse] = useState(null);
  const [loading, setLoading] = useState(false);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    setLoading(true);

    (async () => {
      try {
        const data = await client.request(query, variables);
        setResponse(data);
        setLoading(false);
      } catch (e) {
        setHasError(true);
        setLoading(false);
        console.error(e);
      }
    })();
  }, [query, variables]);
  return [response, loading, hasError];
}

export { client, useGraphql };
