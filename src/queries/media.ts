export const MediaByID = `query Q($mediaId: Int!) {
  media(mediaId: $mediaId) {
    categoryId
    mediaState {
      mediaStateId
      mediaState
    }
    mediaType {
      mediaTypeId
      mediaType
    }
    languageId
    statusInCoo
    synopsis
    startDate
    endDate
    season
    chapterCount
    averageRating
    weightedRating
    mediaVotes
    licensed
    completelyTranslated
    nsfw
    createdAt
    updatedAt
    mediaGenres(first: 50) {
      nodes {
        genreId
        genre {
          genre
          genreDescription
        }
      }
    }
    mediaTags(first: 50) {
      nodes {
        tagId
        tagWeight
        tag {
          tag
          tagDescription
        }
      }
    }
    mediaAuthors(first: 5) {
      nodes {
        authorId
      }
    }
    mediaPublishers(first: 5) {
      nodes {
        publisherId
      }
    }
    mediaRelationsByMedia1Id(first: 20) {
      nodes {
        media1Id
        media2Id
        relationId
      }
    }
    mediaRecommendationsByMedia1Id(first: 20) {
      nodes {
        media1Id
        media2Id
        approvals
      }
    }
    mediaTitles(first: 20) {
      nodes {
        mediaTitleId
        mediaTitle
        languageId
        mediaNameType
      }
    }
    volumes(first: 50) {
      nodes {
        volumeId
        volumeNumber
        volumeName
        chapters(first: 1000) {
          nodes {
            chapterId
            chapterTypeId
            chapterNumber
            absoluteChapterNumber
            chapter
            chapterLength
            releaseDate
            createdAt
            updatedAt
          }
        }
      }
    }
  }
}

query ReleaseById($releaseId: Int!) {
  release(releaseId: $releaseId) {
    chapter {
      chapter
    }
    releaseGroup {
      releaseGroupId
      releaseGroupName
      releaseGroupContact {
        officialWebsite
        twitter
        facebook
        irc
        forum
        discord
        mail
      }
    }
    languageId
    content
    sourceUrl
    translator
    createdAt
  }
}

query ReleaseGroupById($releaseGroupId: Int!) {
  releaseGroup(releaseGroupId: $releaseGroupId) {
    releaseGroupName
    releaseGroupContact {
      officialWebsite
      twitter
      facebook
      irc
      forum
      discord
      mail
    }
  }
}`;
