export const Discover = `fragment MediaDiscoveryInfo on Media {
    mediaId
    categoryId
    mediaStateId
    mediaTypeId
    languageId
    statusInCoo
    synopsis
    startDate
    chapterCount
    weightedRating
    completelyTranslated
    nsfw
    mediaTitles(first: 1, condition: { mediaNameType: MAIN }) {
      nodes {
        mediaTitleId
        mediaTitle
        mediaNameType
      }
    }
    mediaGenres(first: 20) {
      nodes {
        genreId
        genre {
          genre
        }
      }
    }
    mediaAuthors(first: 5) {
      nodes {
        author {
          authoredAuthorNames(first: 5) {
            nodes {
              authorName
              languageId
              authorNameType
            }
          }
        }
      }
    }
    posters(first:1) {
      nodes {
        posterId
        main
        posterUrl
      }
    }
  }

  query Discover(
    $limit: Int!
    $skip: Int!
    $order: [MediaOrderBy!]
    $mediaTypeIds: [Int!]
    $minRating: BigFloat!
    $search: String!
    $tagFilter: [MediaFilter!]
  ) {
    medias(
      first: $limit
      offset: $skip
      orderBy: $order
      filter: {
        mediaTypeId: { in: $mediaTypeIds }
        weightedRating: { greaterThanOrEqualTo: $minRating }
        mediaTitles: { some: { mediaTitle: { includesInsensitive: $search } } }
        and: $tagFilter
      }
    ) {
      nodes {
        ...MediaDiscoveryInfo
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
      totalCount
    }
  }
  `;
