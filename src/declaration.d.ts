import { JSX } from "preact";
export = JSX;
export as namespace JSX;

// css modules declaration
declare module "*.module.css" {
  const styles: { [className: string]: string };
  export default styles;
}
