export type ThresholdUnits = "Pixel" | "Percent";

export interface Threshold {
  unit: ThresholdUnits;
  value: number;
}

const defaultThreshold: Threshold = {
  unit: "Percent",
  value: 0.8,
};

export function parseThreshold(scrollThreshold: string | number): Threshold {
  if (typeof scrollThreshold === "number") {
    return {
      unit: "Percent",
      value: scrollThreshold * 100,
    };
  }

  if (typeof scrollThreshold === "string") {
    if (scrollThreshold.match(/^(\d*(\.\d+)?)px$/)) {
      return {
        unit: "Pixel",
        value: parseFloat(scrollThreshold),
      };
    }

    if (scrollThreshold.match(/^(\d*(\.\d+)?)%$/)) {
      return {
        unit: "Percent",
        value: parseFloat(scrollThreshold),
      };
    }

    console.warn(
      'scrollThreshold format is invalid. Valid formats: "120px", "50%"...',
    );

    return defaultThreshold;
  }

  console.warn("scrollThreshold should be string or number");

  return defaultThreshold;
}
