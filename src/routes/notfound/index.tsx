import { h, FunctionalComponent } from "preact";
import { Link } from "preact-router";
import styles from "../../css/notfound.module.css";
import { Logo } from "../../logo";
const Notfound: FunctionalComponent = () => {
  return (
    <div className={styles.notFound}>
      <Logo />
      <h1>404</h1>
      <p>The page you are looking for doesn&apos;t exist.</p>
      <Link href="/">
        <strong>Back to Home</strong>
      </Link>
    </div>
  );
};

export default Notfound;
