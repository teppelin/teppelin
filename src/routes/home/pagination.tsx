import Paginating from "react-paginating";
import { h, FunctionalComponent } from "preact";
import style from "./pagination.module.css";

interface BrowseProps {
  onPageChange: (n: number) => void;
  limit: number;
  skip: number;
  total: number;
  visiblePages?: number;
  firstLabel?: string;
  lastLabel?: string;
  previousLabel?: string;
  nextLabel?: string;
}

const Pagination: FunctionalComponent<BrowseProps> = ({
  onPageChange,
  limit,
  skip,
  total,
  visiblePages = 5,
  firstLabel = "first",
  lastLabel = "last",
  previousLabel = "<",
  nextLabel = ">",
}) => {
  return (
    <Paginating
      total={total}
      limit={limit}
      pageCount={visiblePages}
      currentPage={skip / limit + 1}
    >
      {({
        pages,
        currentPage,
        hasNextPage,
        hasPreviousPage,
        previousPage,
        nextPage,
        totalPages,
        getPageItemProps,
      }) => (
        <div>
          <a
            {...getPageItemProps({
              pageValue: 1,
              onPageChange,
              className: style.pageItem,
            })}
          >
            {firstLabel}
          </a>

          {hasPreviousPage && (
            <a
              {...getPageItemProps({
                pageValue: previousPage,
                onPageChange,
                className: style.pageItem,
              })}
            >
              {previousLabel}
            </a>
          )}

          {pages.map((page) => {
            let activePage = "";
            if (currentPage === page) {
              activePage = style.pageItemActive;
            }
            return (
              // eslint-disable-next-line react/jsx-key
              <a
                {...getPageItemProps({
                  key: page,
                  pageValue: page,
                  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                  // @ts-ignore
                  className: `${style.pageItem} ${activePage}`,
                  onPageChange,
                })}
              >
                {page}
              </a>
            );
          })}

          {hasNextPage && (
            <a
              {...getPageItemProps({
                pageValue: nextPage,
                onPageChange,
                className: style.pageItem,
              })}
            >
              {nextLabel}
            </a>
          )}

          <a
            {...getPageItemProps({
              pageValue: totalPages,
              onPageChange,
              className: style.pageItem,
            })}
          >
            {lastLabel}
          </a>
        </div>
      )}
    </Paginating>
  );
};

export default Pagination;
