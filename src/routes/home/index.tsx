import { useState, useEffect } from "preact/hooks";
import { h, FunctionalComponent, Fragment } from "preact";
import style from "./home.module.css";
import { client } from "../../client";
import { Discover } from "../../queries";
import Pagination from "./pagination";
import "preact/debug";

interface BrowseProps {
  // eslint-disable-next-line
  media: any[];
}

const Browse: FunctionalComponent<BrowseProps> = ({ media }) => {
  const title = (m) =>
    m.mediaTitles.nodes.find((x) => x.mediaNameType === "MAIN").mediaTitle;

  return (
    <Fragment>
      {media.map((m) => (
        <li key={m.mediaId}>
          <div className={style.coverImg}>
            {m.posters.nodes.length && (
              <img src={m.posters.nodes[0].posterUrl}></img>
            )}
          </div>
          <div>
            {/* uncommenting this breaks everything :pepoThink: */}
            {title(m)}
            <span>
              &nbsp;/ Rating: <b>{parseFloat(m.weightedRating).toFixed(2)}</b>
              &nbsp;
            </span>
            <span>
              Chapters: <b>{m.chapterCount}</b>
            </span>
          </div>
        </li>
      ))}
    </Fragment>
  );
};

const Home: FunctionalComponent = () => {
  const [media, setMedia] = useState([]);
  const [totalCount, setTotalCount] = useState(null);
  // const [pageInfo, setPageInfo] = useState(null);
  const [variables, setVariables] = useState({
    skip: 0,
    limit: 25,
    order: "WEIGHTED_RATING_DESC",
    mediaTypeIds: [1, 2, 3, 4, 5, 7, 8],
    minRating: 3.0,
    tagFilter: [],
    search: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      setIsLoading(true);

      const res = await client.request(Discover, variables);

      setMedia(res.medias.nodes);
      setTotalCount(res.medias.totalCount);
      setIsLoading(false);
    })();
  }, [variables]);

  const pageChangeHandler = (page: number) => {
    console.log(page);
    setVariables((v) => ({ ...v, skip: (page - 1) * v.limit }));
  };

  if (isLoading) return <div>Loading...</div>;

  return (
    <Fragment>
      {/* TODO: Filtering component that modifies other variables */}
      <Browse media={media} />
      <Pagination
        total={totalCount}
        limit={variables.limit}
        onPageChange={pageChangeHandler}
        skip={variables.skip}
      />
    </Fragment>
  );
};

export default Home;

//   const title = (m) =>
//     m.mediaTitles.nodes.find((x) => x.mediaNameType === "MAIN").mediaTitle;

//   return (
//     <InfiniteScroll
//       pageStart={0}
//       loadMore={(p) => setSkip(limit * p)}
//       hasMore={true}
//       loader={
//         <div className="loader" key={0}>
//           Loading ...
//         </div>
//       }
//     >
//       {data.map((m) => (
//         <li key={m.mediaId}>
//           <div className={style.coverImg}>
//             {m.posters.nodes.length && (
//               <img src={m.posters.nodes[0].posterUrl}></img>
//             )}
//           </div>
//           <div className={style.list}>
//             {/* uncommenting this breaks everything :pepoThink: */}
//             {title(m)}
//             <span>
//               &nbsp;/ Rating: <b>{parseFloat(m.weightedRating).toFixed(2)}</b>
//               &nbsp;
//             </span>
//             <span>
//               Chapters: <b>{m.chapterCount}</b>
//             </span>
//           </div>
//         </li>
//       ))}
//     </InfiniteScroll>
//   );
// };

// export default Home;
