import { h, FunctionalComponent } from "preact";
import styles from "../css/components.module.css";

export const Layout: FunctionalComponent = (props) => {
  return (
    <main className={styles.layoutWrapper}>
      <section className={styles.gridWrapper}>{props.children}</section>
    </main>
  );
};
