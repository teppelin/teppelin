import { h, FunctionalComponent } from "preact";
// import { Logo } from "../logo";
import { Route, Router, RouterOnChangeArgs } from "preact-router";

import Home from "../routes/home";
import NotFoundPage from "../routes/notfound";
import { Layout } from "./layout";
import { useEffect } from "preact/hooks";

export const App: FunctionalComponent = () => {
  // eslint-disable-next-line
  let currentUrl: string;
  const handleRoute = (e: RouterOnChangeArgs) => {
    // eslint-disable-next-line
    currentUrl = e.url;
  };

  const dataWorker = new Worker("/dataWorker.js");
  useEffect(() => {
    (async () => {
      dataWorker.postMessage({ method: "update" });
      console.debug("App: update call sent to worker");

      dataWorker.onmessage = (e) => {
        // result.textContent = e.data;
        console.debug("App: message received from worker", e);
      };
    })();
  }, [dataWorker]);

  return (
    <Layout>
      <Router onChange={handleRoute}>
        <Route path="/" component={Home} />
        <NotFoundPage default />
      </Router>
    </Layout>
  );
};
