# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keep a changelog] and this project adheres to [Semantic Versioning][semantic versioning].

## [Unreleased]

---

## [Released]

---

<!-- Links -->

[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html

<!-- Versions -->

[unreleased]: https://code.miraris.moe/teppelin/teppelin/compare/v1.0.0...HEAD
[released]: https://code.miraris.moe/teppelin/teppelin/releases
[0.0.2]: https://code.miraris.moe/teppelin/teppelin/compare/v0.0.1..v0.0.2
[0.0.1]: https://code.miraris.moe/teppelin/teppelin/releases/v0.0.1
