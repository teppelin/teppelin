/* eslint-disable no-undef */
// TODO: https://github.com/vitejs/vite/issues/403
importScripts("https://unpkg.com/idb@latest/build/iife/index-min.js");

const query = `query($tags: Int!, $genres: Int!, $categories: Int!, $relations: Int) {
  tags: tagsList(offset: $tags) {
    tagId
    tag
    tagDescription
  }
  genres: genresList(offset: $genres) {
    genreId
    genre
    genreDescription
  }
  categories: categoriesList(offset: $categories) {
    categoryId
    category
  }
  relations: relationsList(offset: $relations) {
    relationId
    relation
  }
}
`;
const request = (query, variables) =>
  fetch("https://teppel.in/graphql", {
    method: "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify({ query, variables }),
  }).then((x) => x.json());

onmessage = async (e) => {
  console.debug("Worker: Message received from main script", e);
  if (e && e.data && e.data.method in dicts) {
    const r = await dicts[e.data.method]();
    postMessage(r);
  }
};

const dicts = {
  // todo: save media
  update: async () => {
    // const stores = await new Map([
    const stores = [
      ["genres", "genreId"],
      ["tags", "tagId"],
      ["categories", "categoryId"],
      ["relations", "relationId"],
    ];
    // ]);
    // const storeKeys = await stores.keys();
    const db = await idb.openDB("teppelin", 1, {
      upgrade(db) {
        for (const [k, v] of stores) {
          db.createObjectStore(k, { keyPath: v });
        }
      },
    });

    const vs = {};
    console.log(stores);
    for (const [k] of stores) {
      try {
        console.log(k);
        const c = await db.count(k);
        vs[k] = c || 0;
      } catch (e) {
        console.log(e);
      }
    }

    console.debug(vs);

    // could fetch totalCount first and set offsets based on that +
    // optionally skip max offset fetches.
    // However I'm not sure if counting rows is less expensive than
    // the where clauses using row count that postgraphile generates

    const { data } = await request(query, vs);

    for (const k in data) {
      const xs = data[k];
      if (xs.length === 0) {
        console.debug(`0 entries (${k} up to date)`);
        continue;
      }
      const tx = db.transaction(k, "readwrite");
      await Promise.all([...xs.map((v) => tx.store.put(v)), tx.done]);
    }
  },
};
