# Teppelin

[![Build Status](https://drone.miraris.moe/api/badges/teppelin/teppelin/status.svg)](https://drone.miraris.moe/teppelin/teppelin)

## Table of Contents

- [About](#about)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [Contributing](../CONTRIBUTING.md)

## About

The Teppelin Preact web app.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

- [Node.js](https://nodejs.org/en/download/)

### Installing

Run with npm or yarn

```
yarn install
```

then run

```
yarn dev
```

## Usage

Add notes about how to use the system.
