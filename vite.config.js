/* eslint-disable */
// @ts-nocheck
const preactRefresh = require("@prefresh/vite");

const analyze = !!process.env.ANALYZE;

/**
 * @type { import('vite').UserConfig }
 */
const config = {
  alias: {
    react: "preact/compat",
    "react-dom": "preact/compat",
    "react-dom/test-utils": "preact/test-utils",
  },
  jsx: {
    factory: "h",
    fragment: "Fragment",
  },
  plugins: [preactRefresh()],
  rollupInputOptions: {
    plugins: [
      require("@rollup/plugin-replace")({
        "process.env.NODE_ENV": '"production"',
        __DEV__: "false",
      }),
      ...(analyze ? [require("rollup-plugin-visualizer")()] : []),
    ],
  },
};

module.exports = config;
