FROM node:alpine AS builder
WORKDIR /usr/src/app
COPY package*.json \
  yarn.lock \
  index.html \
  vite.config.js \
  ./
COPY . .
RUN apk --no-cache add brotli \
  && yarn install --frozen-lockfile \
  && yarn build \
  && yarn brotli

FROM node:alpine
WORKDIR /app
COPY --from=builder /usr/src/app/dist .
RUN yarn global add sirv-cli
EXPOSE 80
ENTRYPOINT ["sirv"]
CMD ["/app", "-s", "-B", "-q"]
